#!/usr/bin/env python3
from flask import Flask, render_template
from os.path import sep
from base64 import b64encode
from werkzeug.exceptions import HTTPException

app = Flask(__name__)

FALLBACK_ERROR_PAGE = 999

# Set error page contents
ERROR_PAGES = {
    400: ("Bad Request", "looks like you", "want something strange"),
    401: ("Unauthorized", "looks like you are", "not telling me your name"),
    403: ("Forbidden", "looks like you are", "not allowed to enter"),
    404: ("Page Not Found", "looks like you are", "lost in space"),
    405: ("Method Not Allowed", "looks like you", "tried the wrong door"),
    414: ("URI Too Long", "looks like you are", "talking to much"),
    500: ("Internal Server Error", "looks like you are", "out of luck"),
    503: ("Service Unavailable", "looks like you should", "try again later"),
    999: ("Error", "looks like you", "did something wrong"),
}

# configure svg sources
MEDIA = {
    "astronaut_svg": "media%sastronaut.svg" % sep,
    "earth_svg": "media%searth.svg" % sep,
    "moon_svg": "media%smoon.svg" % sep,
    "overlay_stars_svg": "media%soverlay_stars.svg" % sep,
    "rocket_svg": "media%srocket.svg" % sep,
}


# functions
def encode_image(image):
    raw = open(image, "rb").read()  # read file binary
    parsed = b64encode(raw)  # encode to binary b64
    data = parsed.decode("utf-8")  # decode b64 bytes to utf-8 string
    return "data:image/svg+xml;base64,%s" % data


#### main execution ####
# preload svg data
for svg_object in MEDIA:
    MEDIA[svg_object] = encode_image(MEDIA[svg_object])


# define routes
@app.route('/<int:http_error_code>.html')
def show_error_page(http_error_code):
    http_error_message, pretty_message_line1, pretty_message_line2 = ERROR_PAGES.get(
        http_error_code, ERROR_PAGES[FALLBACK_ERROR_PAGE])
    return render_template("error_page.html.j2",
                           **MEDIA,
                           http_error_code=http_error_code,
                           http_error_message=http_error_message,
                           pretty_message_line1=pretty_message_line1,
                           pretty_message_line2=pretty_message_line2)

@app.errorhandler(HTTPException)
def handle_exceptions(http_exception):
    return show_error_page(http_exception.code)


