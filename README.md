# What is this? #

This is a simple flask container hosting custom error pages. It is based on the latest alpine image to be as small as possible.

This container is intended to be used using Traefik to serve the same error pages for all services. It will also register a _catchall_ router with lowest priority of `1` so that any Traefik 404 error also gets caught by this service. This allows for beautiful 404 pages instead of a simple `404 page not found` displayed by Traefik.

The error page design has been written by [_Saleh Riaz_](http://salehriaz.com) and may be found in its original on [their website](http://salehriaz.com/404Page/404.html).

_**Note**: This image has been designed with my personal needs in mind!_

# How to #

1. Remove the middleware label or make sure a middleware called "security" is defined in file configuration
2. Run the container with `docker run -d ttrafelet/traefik_error_pages`
3. Register the middleware `serve-pretty-errors@docker` on any router where you want to catch errors

# Security #

This container will by default run as non-root user `flask`. As a consequence, you may not use ports below `1024` for `$APP_PORT` (default is `5000`)
